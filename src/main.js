import { createApp } from 'vue'
import './assets/css/style.css'
import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'

import Kanal from "./views/Kanal.vue"
import Home from "./views/Home.vue"


const routes = [
    { path: "/", component: Home },
    { path: "/kanal", component: Kanal },
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes
});

import VueSplide from '@splidejs/vue-splide';
createApp(App)
    .use(router)
    .use(VueSplide)
    .mount('#app')